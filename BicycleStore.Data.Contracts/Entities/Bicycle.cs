﻿using System;
using System.Collections.Generic;
using BicycleStore.Data.Contracts.Enums;

namespace BicycleStore.Data.Contracts.Entities
{
    public class Bicycle
    {
        public Guid Id { get; set; }
        public string Name { get; set; }        
        public virtual BicycleType  BicycleType { get; set; }
        public Guid BicycleTypeId { get; set; }
        public Sex Sex { get; set; }
        public float WheelDiameter { get; set; }
        public virtual Brand Brand { get; set; }
        public Guid BrandId { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }                       
        public virtual List<Photo> AllPhotos { get; set; }
        public virtual List<Configuration> Configurations { get; set; }
    }
}