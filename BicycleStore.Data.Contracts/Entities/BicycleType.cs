﻿using System;

namespace BicycleStore.Data.Contracts.Entities
{
    public class BicycleType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}