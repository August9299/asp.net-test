﻿using System;

namespace BicycleStore.Data.Contracts.Entities
{
    public class Brand
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        
    }
}