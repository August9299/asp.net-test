﻿using System;

namespace BicycleStore.Data.Contracts.Entities
{
    public class Configuration
    {
        public Guid Id { get; set; }
        public virtual Bicycle Bicycle { get; set; }
        public Guid BicycleId { get; set; }
        public float FrameSize { get; set; }
        public virtual Color Color { get; set; }
        public Guid ColorId { get; set; }
        public int Availability { get; set; }
    }
}