﻿using System;

namespace BicycleStore.Data.Contracts.Entities
{
    public class Feedback
    {
        public Guid Id { get; set; }
        public virtual User User { get; set; }
        public Guid UserId { get; set; }
        public virtual Bicycle Bicycle { get; set; }
        public Guid BicycleId { get; set; }
        public DateTime PostedTime { get; set; }
        public string Text { get; set; }
        public int Rating { get; set; }
        public bool Approved { get; set; }
    }
}