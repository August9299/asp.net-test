﻿using System;

namespace BicycleStore.Data.Contracts.Entities
{
    public class Order
    {
        public Guid Id { get; set; }
        public virtual Configuration Configuration { get; set; }
        public Guid ConfigurationId { get; set; }
        public virtual User User { get; set; }
        public Guid UserId { get; set; }
        public DateTime OrderTime { get; set; }
        public string Adress { get; set; }
        public bool ReadyToShip { get; set; }
        public decimal Price { get; set; }
    }
}