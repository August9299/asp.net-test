﻿using System;

namespace BicycleStore.Data.Contracts.Entities
{
    public class Photo
    {
        public Guid Id { get; set; }
        public virtual Bicycle Bicycle { get; set; }
        public Guid BicycleId { get; set; }
        public bool IsMain { get; set; }
    }
}