﻿namespace BicycleStore.Data.Contracts.Enums
{
    public enum Sex
    {
        Male = 0,
        Female = 1,
        Unisex = 2
    }
}