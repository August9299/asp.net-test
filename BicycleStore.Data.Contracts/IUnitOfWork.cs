﻿using System;
using System.Data.Entity;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        IBicycleRepository GetBicycleRepository();
        IBicycleTypeRepository GetBicycleTypeRepository();
        IBrandRepository GetBrandRepository();
        IConfigurationRepository GetConfigurationRepository();
        IPhotoRepository GetPhotoRepository();
        void SaveChanges();
    }
}