﻿using System.Collections.Generic;
using BicycleStore.Data.Contracts.Entities;

namespace BicycleStore.Data.Contracts.Repositories
{
    public interface IBicycleRepository : IRepository<Bicycle>
    {               
    }
}