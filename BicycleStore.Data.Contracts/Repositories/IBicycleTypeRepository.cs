﻿using BicycleStore.Data.Contracts.Entities;

namespace BicycleStore.Data.Contracts.Repositories
{
    public interface IBicycleTypeRepository : IRepository<BicycleType>
    {        
    }
}