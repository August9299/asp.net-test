﻿using BicycleStore.Data.Contracts.Entities;

namespace BicycleStore.Data.Contracts.Repositories
{
    public interface IConfigurationRepository : IRepository<Configuration>
    {        
    }
}