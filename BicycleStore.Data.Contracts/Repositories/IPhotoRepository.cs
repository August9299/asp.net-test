﻿using System;
using System.Collections.Generic;
using BicycleStore.Data.Contracts.Entities;

namespace BicycleStore.Data.Contracts.Repositories
{
    public interface IPhotoRepository : IRepository<Photo>
    {
        IEnumerable<Photo> GetAllPhotosByBicycle(Guid bicycleId);
    }
}