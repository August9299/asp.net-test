﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BicycleStore.Data.Contracts.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        TEntity GetById(Guid id);
        void Insert(TEntity item);
        void Update(TEntity item);
        void Delete(TEntity item);
    }
}