﻿using System;
using BicycleStore.Data.Contracts.Enums;

namespace BicycleStore.Data.Contracts.RequestModels
{
    public class BicycleRequestModel
    {        
        public Guid Id { get; set; }
        
        public string Name { get; set; }
     
        public Guid BicycleTypeId { get; set; }
        
        public Sex Sex { get; set; }
        
        public float WheelDiameter { get; set; }
        
        public Guid BrandId { get; set; }
        
        public decimal Price { get; set; }

        public string Description { get; set; }                        
    }
}