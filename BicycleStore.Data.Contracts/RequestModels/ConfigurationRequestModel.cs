﻿using System;

namespace BicycleStore.Data.Contracts.RequestModels
{
    public class ConfigurationRequestModel
    {
        public Guid Id { get; set; }        
        public Guid BicycleId { get; set; }
        public float FrameSize { get; set; }                
        public int Availability { get; set; }
    }
}