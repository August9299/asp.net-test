﻿using System;
using System.IO;

namespace BicycleStore.Data.Contracts.RequestModels
{
    public class PhotoRequestModel
    {
        public Guid Id { get; set; }
        public Stream InputStream { get; set; }
        public Guid BicycleId { get; set; }
        public string SavePath { get; set; }
        public bool IsMain { get; set; }
    }
}