﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using BicycleStore.Data.Contracts.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BicycleStore.Data.DAL
{
    public class BicycleStoreDbContext : IdentityDbContext<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public BicycleStoreDbContext() : base("name=BicycleShopConnection")
        {            
        }
        public DbSet<Bicycle> Bicycles { get; set; }
        public DbSet<BicycleType> BicycleTypes { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Photo> Photos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new StudentEntityConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public static BicycleStoreDbContext Create()
        {
            return new BicycleStoreDbContext();
        }
    }
}