﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace BicycleStore.Data.DAL
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            using (var client = new SmtpClient())
            {
                // TODO: move to config
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;                
                client.Credentials = new NetworkCredential("vladimircheetah64@gmail.com", "qaswedfr4");
                client.EnableSsl = true;                

                var from = new MailAddress("vladimircheetah64@gmail.com");
                var to = new MailAddress(message.Destination);

                var mailMessage = new MailMessage(from, to)
                {
                    Subject = message.Subject,
                    Body = message.Body,
                    IsBodyHtml = true
                };
                try
                {
                    client.Send(mailMessage);
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"Exception: {e.Message}");
                    Debug.WriteLine($"Inner exception: {e.InnerException?.Message}");                    
                }
            }
        }
    }
}