﻿using System;
using BicycleStore.Data.Contracts.Entities;
using Microsoft.AspNet.Identity;

namespace BicycleStore.Data.DAL.Identity
{
    public class BicycleStoreRoleManager : RoleManager<Role, Guid>
    {
        public BicycleStoreRoleManager(IRoleStore<Role, Guid> store) : base(store)
        {
        }
    }
}