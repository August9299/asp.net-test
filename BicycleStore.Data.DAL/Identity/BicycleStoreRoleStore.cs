﻿using System;
using System.Data.Entity;
using BicycleStore.Data.Contracts.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BicycleStore.Data.DAL.Identity
{
    public class BicycleStoreRoleStore : RoleStore<Role, Guid, UserRole>
    {
        public BicycleStoreRoleStore(DbContext context) : base(context)
        {
        }
    }
}