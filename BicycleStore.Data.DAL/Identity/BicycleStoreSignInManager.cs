﻿using System;
using BicycleStore.Data.Contracts.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace BicycleStore.Data.DAL.Identity
{
    public class BicycleStoreSignInManager : SignInManager<User, Guid>
    {
        public BicycleStoreSignInManager(UserManager<User, Guid> userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager)
        {
        }

        public static BicycleStoreSignInManager Create(IdentityFactoryOptions<BicycleStoreSignInManager> options, IOwinContext context)
        {
            return new BicycleStoreSignInManager(context.GetUserManager<BicycleStoreUserManager>(), context.Authentication);
        }
    }
}