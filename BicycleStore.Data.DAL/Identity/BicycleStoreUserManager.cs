﻿using System;
using System.Threading.Tasks;
using BicycleStore.Data.Contracts.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace BicycleStore.Data.DAL.Identity
{
    public class BicycleStoreUserManager : UserManager<User, Guid>
    {
        public BicycleStoreUserManager(IUserStore<User, Guid> store) : base(store)
        {
        }

        public static BicycleStoreUserManager Create(IdentityFactoryOptions<BicycleStoreUserManager> options, IOwinContext context)
        {
            var manager = new BicycleStoreUserManager(new BicycleStoreUserStore(context.Get<BicycleStoreDbContext>()));

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<User, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,                
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            manager.EmailService = new EmailService();            
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<User, Guid>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }    
}