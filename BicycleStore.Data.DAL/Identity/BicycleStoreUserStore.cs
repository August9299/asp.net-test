﻿using System;
using System.Data.Entity;
using BicycleStore.Data.Contracts.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BicycleStore.Data.DAL.Identity
{
    public class BicycleStoreUserStore : UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public BicycleStoreUserStore(DbContext context) : base(context)
        {
        }
    }
}