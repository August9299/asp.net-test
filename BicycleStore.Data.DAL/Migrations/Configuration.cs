using System.Collections.Generic;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.DAL.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;

namespace BicycleStore.Data.DAL.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<BicycleStoreDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BicycleStoreDbContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
            SeedColors(context);
            SeedBrands(context);
            SeedBicycleTypes(context);
            context.SaveChanges();
        }

        private void SeedBicycleTypes(BicycleStoreDbContext context)
        {
            BicycleType[] types =
            {
                new BicycleType {Name = "MTB"},
                new BicycleType {Name = "Street bike"},
                new BicycleType {Name = "BMX"},
            };
            foreach (var type in types)
            {
                var existing = context.BicycleTypes.FirstOrDefault(t => t.Name == type.Name);
                if (existing == null)
                {
                    type.Id = Guid.NewGuid();
                    context.BicycleTypes.Add(type);
                }
            }
        }

        private void SeedBrands(BicycleStoreDbContext context)
        {
            Brand[] brands = 
            {
                new Brand {Name = "GT"}, 
                new Brand {Name = "Corratec"}, 
                new Brand {Name = "Cannondale"}, 
                new Brand {Name = "Aist"}, 
            };
            foreach (var brand in brands)
            {
                var existing = context.Brands.FirstOrDefault(b => b.Name == brand.Name);
                if (existing == null)
                {
                    brand.Id = Guid.NewGuid();
                    context.Brands.Add(brand);
                }
            }
        }

        private void SeedColors(BicycleStoreDbContext context)
        {
            var id = new Guid("4DC578F5-16D5-4E82-A9F7-88672ADF7F53");
            if (context.Colors.Find(id) == null)
            {
                context.Colors.Add(new Color
                {
                    Name = "Black",
                    Id = id
                });
            }
        }

        private void SeedUsers(BicycleStoreDbContext context)
        {
            var userManager = new BicycleStoreUserManager(new BicycleStoreUserStore(context));
            userManager.PasswordValidator = new MinimumLengthValidator(1);

            if (userManager.FindByName("admin") != null) return;
            var administrator = new User
            {
                Id = Guid.NewGuid(),
                UserName = "admin",
                RegistrationDate = DateTime.Now,
                EmailConfirmed = true
            };
            userManager.Create(administrator, "admin");
            userManager.AddToRole(administrator.Id, "Administrator");
        }

        private void SeedRoles(BicycleStoreDbContext context)
        {
            var roleManager = new BicycleStoreRoleManager(new BicycleStoreRoleStore(context));
            var roleList = new List<Role>
            {
                new Role {Id = Guid.NewGuid(), Name = "Administrator"},
                new Role {Id = Guid.NewGuid(), Name = "Operator"},
                new Role {Id = Guid.NewGuid(), Name = "Moderator"},
                new Role {Id = Guid.NewGuid(), Name = "Buyer"}
            };

            foreach (var role in roleList)
            {
                if (roleManager.FindByName(role.Name) == null)
                {
                    roleManager.Create(role);
                }
            }
        }
    }
}