﻿using System.Collections.Generic;
using System.Linq;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.DAL.Repositoreies
{
    public class BicycleRepository : Repository<Bicycle>, IBicycleRepository
    {       
        public BicycleRepository(BicycleStoreDbContext dbContext) : base(dbContext)
        {            
        }      
    }
}