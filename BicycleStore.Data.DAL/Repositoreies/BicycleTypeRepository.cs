﻿using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.DAL.Repositoreies
{
    public class BicycleTypeRepository : Repository<BicycleType>, IBicycleTypeRepository
    {
        public BicycleTypeRepository(BicycleStoreDbContext dbContext) : base(dbContext)
        {
        }
    }
}