﻿using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.DAL.Repositoreies
{    
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
        public BrandRepository(BicycleStoreDbContext dbContext) : base(dbContext)
        {
        }
    }
}