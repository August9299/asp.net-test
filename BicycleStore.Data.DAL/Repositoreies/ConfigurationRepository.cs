﻿using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.DAL.Repositoreies
{
    public class ConfigurationRepository : Repository<Configuration>, IConfigurationRepository
    {
        public ConfigurationRepository(BicycleStoreDbContext dbContext) : base(dbContext)
        {
        }
    }
}