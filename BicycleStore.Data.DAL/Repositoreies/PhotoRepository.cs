﻿using System;
using System.Collections.Generic;
using System.Linq;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.DAL.Repositoreies
{
    public class PhotoRepository : Repository<Photo>, IPhotoRepository
    {
        public PhotoRepository(BicycleStoreDbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Photo> GetAllPhotosByBicycle(Guid bicycleId)
        {
            return GetAll().Where(photo => photo.BicycleId == bicycleId);
        }
    }
}