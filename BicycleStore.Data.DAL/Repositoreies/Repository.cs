﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BicycleStore.Data.Contracts.Repositories;

namespace BicycleStore.Data.DAL.Repositoreies
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected BicycleStoreDbContext DbContext { get; }    
        protected DbSet<TEntity> DbSet { get; }

        public Repository(BicycleStoreDbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public virtual TEntity GetById(Guid id)
        {
            return DbSet.Find(id);
        }

        public virtual void Insert(TEntity entity)
        {
            var entityEntry = DbContext.Entry(entity);
            if (entityEntry.State != EntityState.Detached)
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }

        public virtual void Update(TEntity entity)
        {
            var entityEntry = DbContext.Entry(entity);
            if (entityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            entityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(TEntity entity)
        {
            var entityEntry = DbContext.Entry(entity);
            if (entityEntry.State != EntityState.Deleted)
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }
    }
}