﻿using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Repositories;
using BicycleStore.Data.DAL.Repositoreies;


namespace BicycleStore.Data.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BicycleStoreDbContext _dbContext;
        private IBicycleRepository _bicycleRepository;
        private IBicycleTypeRepository _bicycleTypeRepository;
        private IBrandRepository _brandRepository;
        private IConfigurationRepository _configurationRepository;
        private IPhotoRepository _photoRepository;

        public UnitOfWork()
        {
            _dbContext = new BicycleStoreDbContext();
        }

        public IBicycleRepository GetBicycleRepository()
        {
            return _bicycleRepository ?? (_bicycleRepository = new BicycleRepository(_dbContext));
        }

        public IBicycleTypeRepository GetBicycleTypeRepository()
        {
            return _bicycleTypeRepository ?? (_bicycleTypeRepository = new BicycleTypeRepository(_dbContext));
        }

        public IBrandRepository GetBrandRepository()
        {
            return _brandRepository ?? (_brandRepository = new BrandRepository(_dbContext));
        }

        public IConfigurationRepository GetConfigurationRepository()
        {
            return _configurationRepository ?? (_configurationRepository = new ConfigurationRepository(_dbContext));
        }

        public IPhotoRepository GetPhotoRepository()
        {
            return _photoRepository ?? (_photoRepository = new PhotoRepository(_dbContext));
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }
    }
}