﻿using System;
using AutoMapper;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;

namespace BicycleStore.Logic.BLL.Mappings
{
    public class MainBllProfile : Profile
    {
        public MainBllProfile()
        {
            CreateMap<Bicycle, BicycleDto>();
            CreateMap<Brand, BrandDto>();            
            CreateMap<BicycleType, BicycleTypeDto>();            
            CreateMap<Photo, PhotoDto>();
            CreateMap<Configuration, ConfigurationDto>();
            CreateMap<Color, ColorDto>();
            CreateMap<BicycleRequestModel, Bicycle>();
            CreateMap<ConfigurationRequestModel, Configuration>();
            CreateMap<PhotoRequestModel, Photo>().ForMember(photo => photo.BicycleId, opts=>opts.Condition(model => model.BicycleId != Guid.Empty));
        }
    }
}