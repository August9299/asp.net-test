﻿using BicycleStore.Data.Contracts;

namespace BicycleStore.Logic.BLL.Services
{
    public class BaseService
    {
        public IUnitOfWork UnitOfWork { get; }

        public BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }
    }
}