﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.Logic.BLL.Services
{
    public class BicycleService : BaseService, IBicycleService
    {
        public BicycleService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IEnumerable<BicycleDto> GetAllBicycles()
        {            
            var bicycles = UnitOfWork.GetBicycleRepository().GetAll().ToList(); // TODO: exclude configurations
            return bicycles.Select(Mapper.Map<Bicycle, BicycleDto>);            
        }

        public BicycleDto GetBicycleById(Guid? id)
        {
            if (id == null)
            {
                return null;
            }
            var bicycle = UnitOfWork.GetBicycleRepository().GetById((Guid) id);
            return Mapper.Map<Bicycle, BicycleDto>(bicycle);
        }

        public BicycleDto SaveBicycle(BicycleRequestModel requestModel)
        {           
            Bicycle bicycleEntity = UnitOfWork.GetBicycleRepository().GetById(requestModel.Id);            
            if (bicycleEntity == null)
            {
                bicycleEntity = Mapper.Map<BicycleRequestModel, Bicycle>(requestModel);
                bicycleEntity.Id = Guid.NewGuid();                
                UnitOfWork.GetBicycleRepository().Insert(bicycleEntity);
            }
            else
            {                
                bicycleEntity = Mapper.Map<BicycleRequestModel, Bicycle>(requestModel, bicycleEntity);
                UnitOfWork.GetBicycleRepository().Update(bicycleEntity);
            }
            UnitOfWork.SaveChanges();
            return Mapper.Map<Bicycle, BicycleDto>(bicycleEntity);
        }

        public void DeleteBicycle(Guid? bicycleId)
        {
            if (bicycleId == null)
            {
                return;
            }
            var existingBicycleEntity = UnitOfWork.GetBicycleRepository().GetById((Guid)bicycleId);
            if (existingBicycleEntity != null)
            {
                UnitOfWork.GetBicycleRepository().Delete(existingBicycleEntity);
                UnitOfWork.SaveChanges();
            }
        }
    }
}