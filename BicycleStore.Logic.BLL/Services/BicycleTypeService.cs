﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.Repositories;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.Logic.BLL.Services
{
    public class BicycleTypeService : BaseService, IBicycleTypeService
    {
        public BicycleTypeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IEnumerable<BicycleTypeDto> GetAllBicycleTypes()
        {
            var types = UnitOfWork.GetBicycleTypeRepository().GetAll();
            return types.Select(Mapper.Map<BicycleType, BicycleTypeDto>);
        }
    }
}