﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.Logic.BLL.Services
{
    public class BrandService : BaseService, IBrandService
    {
        public BrandService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IEnumerable<BrandDto> GetAllBrands()
        {
            var brands = UnitOfWork.GetBrandRepository().GetAll();
            return brands.Select(Mapper.Map<Brand, BrandDto>);
        }
    }
}