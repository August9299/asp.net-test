﻿using System;
using AutoMapper;
using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Constants;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.Logic.BLL.Services
{
    public class ConfigurationService : BaseService, IConfigurationService
    {
        public ConfigurationService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public ConfigurationDto GetConfigurationById(Guid? id)
        {
            if (id == null)
            {
                return null;
            }
            var configuration = UnitOfWork.GetConfigurationRepository().GetById((Guid) id);
            return Mapper.Map<Configuration, ConfigurationDto>(configuration);
        }

        public ConfigurationDto SaveConfiguration(ConfigurationRequestModel requestModel)
        {
            Configuration configurationEntity = UnitOfWork.GetConfigurationRepository().GetById(requestModel.Id);
            if (configurationEntity == null)
            {
                configurationEntity = Mapper.Map<ConfigurationRequestModel, Configuration>(requestModel);
                configurationEntity.Id = Guid.NewGuid();
                CheckForColor(configurationEntity);
                UnitOfWork.GetConfigurationRepository().Insert(configurationEntity);
            }
            else
            {
                configurationEntity = Mapper.Map<ConfigurationRequestModel, Configuration>(requestModel, configurationEntity);
                CheckForColor(configurationEntity);
                UnitOfWork.GetConfigurationRepository().Update(configurationEntity);
            }
            UnitOfWork.SaveChanges();
            return Mapper.Map<Configuration, ConfigurationDto>(configurationEntity);
        }

        public void DeleteConfiguration(Guid? id)
        {
            if (id == null)
            {
                return;
            }
            var existingBicycleEntity = UnitOfWork.GetConfigurationRepository().GetById((Guid)id);
            if (existingBicycleEntity != null)
            {
                UnitOfWork.GetConfigurationRepository().Delete(existingBicycleEntity);
                UnitOfWork.SaveChanges();
            }
        }

        private void CheckForColor(Configuration configurationEntity)
        {
            if (configurationEntity.ColorId == Guid.Empty)
            {
                configurationEntity.ColorId = Colors.Black;
            }
        }
    }
}