﻿using System;
using System.IO;
using System.Linq;
using AutoMapper;
using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.Logic.BLL.Services
{
    public class PhotoService : BaseService, IPhotoService
    {
        public PhotoService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public PhotoDto SavePhoto(PhotoRequestModel model)
        {
            Photo photoEntity = UnitOfWork.GetPhotoRepository().GetById(model.Id);
            if (photoEntity == null)
            {
                var newId = Guid.NewGuid();
                string path = model.SavePath;
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }                
                using (var fileStream = File.Create($"{path}/{newId}.jpg"))
                {
                    model.InputStream.Seek(0, SeekOrigin.Begin);
                    model.InputStream.CopyTo(fileStream);
                    fileStream.Close();
                }
                photoEntity = Mapper.Map<PhotoRequestModel, Photo>(model);
                photoEntity.Id = newId;                
                UnitOfWork.GetPhotoRepository().Insert(photoEntity);
                UnitOfWork.SaveChanges();
            }
            else
            {                
                photoEntity = Mapper.Map<PhotoRequestModel, Photo>(model, photoEntity);
                if (photoEntity.IsMain)
                {
                    var otherPhotos = UnitOfWork.GetPhotoRepository().GetAll()                        
                        .Where(photo => photo.BicycleId == photoEntity.BicycleId && photo.IsMain && photo.Id != photoEntity.Id)
                        .ToList();

                    foreach (var photo in otherPhotos)
                    {
                        photo.IsMain = false;
                    }
                }
                UnitOfWork.GetPhotoRepository().Update(photoEntity);
            }
            UnitOfWork.SaveChanges();
            return Mapper.Map<Photo, PhotoDto>(photoEntity);
        }
    }
}