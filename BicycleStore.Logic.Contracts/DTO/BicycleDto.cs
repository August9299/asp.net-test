﻿using System;
using System.Collections.Generic;
using BicycleStore.Data.Contracts.Enums;

namespace BicycleStore.Logic.Contracts.DTO
{
    public class BicycleDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public BicycleTypeDto BicycleType { get; set; }
        public Sex Sex { get; set; }
        public float WheelDiameter { get; set; }
        public BrandDto Brand { get; set; } 
        public decimal Price { get; set; }
        public string Description { get; set; }        
        public IEnumerable<PhotoDto> AllPhotos { get; set; }
        public IEnumerable<ConfigurationDto>  Configurations { get; set; }
    }
}