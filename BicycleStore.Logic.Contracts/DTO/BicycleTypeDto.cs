﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BicycleStore.Logic.Contracts.DTO
{
    public class BicycleTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
