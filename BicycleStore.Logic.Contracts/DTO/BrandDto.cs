﻿using System;

namespace BicycleStore.Logic.Contracts.DTO
{
    public class BrandDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}