﻿using System;

namespace BicycleStore.Logic.Contracts.DTO
{
    public class ColorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}