﻿using System;

namespace BicycleStore.Logic.Contracts.DTO
{
    public class ConfigurationDto
    {
        public Guid Id { get; set; }
        public virtual BicycleDto Bicycle { get; set; }
        public float FrameSize { get; set; }
        public virtual ColorDto Color { get; set; }
        public int Availability { get; set; }
    }
}