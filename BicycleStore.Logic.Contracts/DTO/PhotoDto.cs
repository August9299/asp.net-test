﻿using System;

namespace BicycleStore.Logic.Contracts.DTO
{
    public class PhotoDto
    {
        public Guid Id { get; set; }
        public Guid BicycleId { get; set; }
        public bool IsMain { get; set; }
    }
}