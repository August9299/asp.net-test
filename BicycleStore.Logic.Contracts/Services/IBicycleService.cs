﻿using System;
using System.Collections.Generic;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;

namespace BicycleStore.Logic.Contracts.Services
{
    public interface IBicycleService
    {
        IEnumerable<BicycleDto> GetAllBicycles();
        BicycleDto GetBicycleById(Guid? id);

        BicycleDto SaveBicycle(BicycleRequestModel requestModel);
        void DeleteBicycle(Guid? bicycleId);
    }
}