﻿using System.Collections.Generic;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Logic.Contracts.DTO;

namespace BicycleStore.Logic.Contracts.Services
{
    public interface IBicycleTypeService
    {
        IEnumerable<BicycleTypeDto> GetAllBicycleTypes();
    }
}