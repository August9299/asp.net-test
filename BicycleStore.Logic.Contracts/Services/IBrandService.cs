﻿using System.Collections.Generic;
using BicycleStore.Logic.Contracts.DTO;

namespace BicycleStore.Logic.Contracts.Services
{
    public interface IBrandService
    {
        IEnumerable<BrandDto> GetAllBrands();
    }
}