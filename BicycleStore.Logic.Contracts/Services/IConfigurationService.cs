﻿using System;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;

namespace BicycleStore.Logic.Contracts.Services
{
    public interface IConfigurationService
    {
        ConfigurationDto GetConfigurationById(Guid? id);

        ConfigurationDto SaveConfiguration(ConfigurationRequestModel model);
        void DeleteConfiguration(Guid? id);
    }
}