﻿using System;
using System.Web;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.DTO;

namespace BicycleStore.Logic.Contracts.Services
{
    public interface IPhotoService
    {        
        PhotoDto SavePhoto(PhotoRequestModel model);
    }
}