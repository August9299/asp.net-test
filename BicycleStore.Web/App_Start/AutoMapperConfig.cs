﻿using AutoMapper;

namespace BicycleStore.Web
{
    public class AutoMapperConfig
    {
        public static void AddProfiles()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Logic.BLL.Mappings.MainBllProfile>();
                cfg.AddProfile<Mappings.MainWebProfile>();
            });
        }
    }
}