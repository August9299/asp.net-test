﻿using System.Data.Entity;

namespace BicycleStore.Web
{
    public class DatabaseConfig
    {
        public static void Initialize()
        {
            Database.SetInitializer(new DbContextInitializer());
        }
    }
}