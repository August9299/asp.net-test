﻿using System.Data.Entity;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.DAL;
using BicycleStore.Data.DAL.Identity;
using Microsoft.AspNet.Identity;

namespace BicycleStore.Web
{
    public class DbContextInitializer : MigrateDatabaseToLatestVersion<BicycleStoreDbContext, Data.DAL.Migrations.Configuration>
    {        
    }
}