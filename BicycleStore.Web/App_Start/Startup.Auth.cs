﻿using System;
using BicycleStore.Data.Contracts.Entities;
using BicycleStore.Data.DAL;
using BicycleStore.Data.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace BicycleStore.Web
{
    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(BicycleStoreDbContext.Create);
            app.CreatePerOwinContext<BicycleStoreUserManager>(BicycleStoreUserManager.Create);
            app.CreatePerOwinContext<BicycleStoreSignInManager>(BicycleStoreSignInManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<BicycleStoreUserManager, User, Guid>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                        getUserIdCallback: identity => Guid.Parse(identity.GetUserId()))
                }
            });
        }
    }
}