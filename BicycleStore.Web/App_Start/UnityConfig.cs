using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using BicycleStore.Data.Contracts;
using BicycleStore.Data.Contracts.Repositories;
using BicycleStore.Logic.Contracts.Services;
using BicycleStore.Data.DAL;
using BicycleStore.Logic.BLL.Services;
using BicycleStore.Data.DAL.Repositoreies;
using System;

namespace BicycleStore.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents(UnityContainer container)
        {            
            container.RegisterType<IUnitOfWork, UnitOfWork>(new PerRequestLifetimeManager());
            container.RegisterType<IBicycleService, BicycleService>();            
            container.RegisterType<IBicycleTypeService, BicycleTypeService>();
            container.RegisterType<IBrandService, BrandService>();
            container.RegisterType<IConfigurationService, ConfigurationService>();
            container.RegisterType<IPhotoService, PhotoService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() => {
            var container = new UnityContainer();
            RegisterComponents(container);
            return container;
        });

        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
    }
}