﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.BLL.Services;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;
using BicycleStore.Web.Models;

namespace BicycleStore.Web.Controllers
{
    [Authorize]
    public class AdminPanelController : BaseMvcController
    {
       
        [Authorize(Roles = "Administrator, Moderator, Operator")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Operator")]
        public ActionResult Bicycles()
        {
            var bicycleDtos = BicycleService.GetAllBicycles();
            throw new NullReferenceException("awdawd");
            return View(bicycleDtos.Select(Mapper.Map<BicycleDto, BicycleViewModel>));
        }

        [Authorize(Roles = "Administrator, Operator")]
        public ActionResult EditBicycle(Guid? id)
        {
            var model = new BicycleViewModel();

            var existingBicycle = BicycleService.GetBicycleById(id);
            if (existingBicycle != null)
            {
                model = Mapper.Map(existingBicycle, model);
                model.IsAdding = false;
            }
            else
            {
                model.IsAdding = true;
            }           

            model.AllBicycleTypes = BicycleTypeService.GetAllBicycleTypes()
                .Select(Mapper.Map<BicycleTypeDto, BicycleTypeViewModel>);
            model.AllBrands = BrandService.GetAllBrands().Select(Mapper.Map<BrandDto, BrandViewModel>);

            return View(model);
        }

        [Authorize(Roles = "Administrator, Operator")]
        public ActionResult EditConfiguration(Guid? id, Guid? bicycleId)
        {
            if (bicycleId == null)
            {
                return HttpNotFound();
            }
            var model = new ConfigurationViewModel { BicycleId = (Guid)bicycleId };

            var existingConfiguration = ConfigurationService.GetConfigurationById(id);
            if (existingConfiguration != null)
            {
                model = Mapper.Map(existingConfiguration, model);
                model.IsAdding = false;
            }
            else
            {
                model.IsAdding = true;
            }

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, Operator")]
        public ActionResult UploadImage(HttpPostedFileBase file, Guid bicycleId)
        {
            try
            {
                PhotoRequestModel model = new PhotoRequestModel
                {
                    BicycleId = bicycleId,
                    InputStream = file.InputStream,
                    SavePath = Server.MapPath($"~/Images/{bicycleId}")
                };
                var photoDto = PhotoService.SavePhoto(model);
                return Json(new {imageUrl = $"{photoDto.BicycleId}/{photoDto.Id}.jpg"});
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception: {e.Message}");
                Debug.WriteLine($"Inner: {e.InnerException?.Message}");
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);                
            }            
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, Operator")]
        public ActionResult MakeMainPhoto(Guid id)
        {
            try
            {
                var model = new PhotoRequestModel
                {
                    Id = id,
                    IsMain = true
                };
                PhotoService.SavePhoto(model);                
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception: {e.Message}");
                Debug.WriteLine($"Inner: {e.InnerException?.Message}");
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        public AdminPanelController(IBicycleService bicycleService, IBicycleTypeService bicycleTypeService, IBrandService brandService, IConfigurationService configurationService, IPhotoService photoService) : base(bicycleService, bicycleTypeService, brandService, configurationService, photoService)
        {
        }
    }
}