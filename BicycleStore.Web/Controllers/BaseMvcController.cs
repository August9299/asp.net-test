﻿using System.Web;
using System.Web.Mvc;
using BicycleStore.Data.DAL.Identity;
using BicycleStore.Logic.Contracts.Services;
using Common.Logging;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace BicycleStore.Web.Controllers
{
    public class BaseMvcController : Controller
    {
        protected IBicycleService BicycleService { get; }
        protected IBicycleTypeService BicycleTypeService { get; }
        protected IBrandService BrandService { get; }
        protected IConfigurationService ConfigurationService { get; }
        protected IPhotoService PhotoService { get; }

        protected static ILog Log => LogManager.GetLogger<BaseMvcController>();

        public BaseMvcController(IBicycleService bicycleService, IBicycleTypeService bicycleTypeService,
            IBrandService brandService, IConfigurationService configurationService, IPhotoService photoService)
        {
            BicycleService = bicycleService;
            BicycleTypeService = bicycleTypeService;
            BrandService = brandService;
            ConfigurationService = configurationService;
            PhotoService = photoService;
        }

        protected BicycleStoreSignInManager _signInManager;
        protected BicycleStoreUserManager _userManager;
        protected BicycleStoreRoleManager _roleManager;

        protected BicycleStoreSignInManager SignInManager => _signInManager ??
                                                          (_signInManager = HttpContext.GetOwinContext().Get<BicycleStoreSignInManager>());

        protected BicycleStoreUserManager UserManager => _userManager ??
                                                      (_userManager = HttpContext.GetOwinContext().GetUserManager<BicycleStoreUserManager>());

        protected BicycleStoreRoleManager RoleManager => _roleManager ??
                                                      (_roleManager = HttpContext.GetOwinContext().Get<BicycleStoreRoleManager>());

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        protected void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}