﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Logic.Contracts.Services;
using BicycleStore.Web.Models;

namespace BicycleStore.Web.Controllers
{
    public class HomeController : BaseMvcController
    {
        public ActionResult Index()
        {
            var bikes = BicycleService.GetAllBicycles().Select(Mapper.Map<BicycleDto, BicycleViewModel>);            
            return View(bikes);
        }


        public HomeController(IBicycleService bicycleService, IBicycleTypeService bicycleTypeService, IBrandService brandService, IConfigurationService configurationService, IPhotoService photoService) : base(bicycleService, bicycleTypeService, brandService, configurationService, photoService)
        {
        }
    }
}
