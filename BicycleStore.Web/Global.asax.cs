﻿using System;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Common.Logging;

namespace BicycleStore.Web
{
    public class WebApplication : System.Web.HttpApplication
    {
        protected ILog Logger => LogManager.GetLogger<WebApplication>();

        protected void Application_Start()
        {            
            AreaRegistration.RegisterAllAreas();            
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.AddProfiles();
            DatabaseConfig.Initialize();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();            
            Logger.Error($"Unhandled exception: {exc.GetType()} : {exc.Message}");
            Server.ClearError();
        }
    }
}
