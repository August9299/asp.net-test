﻿using AutoMapper;
using BicycleStore.Logic.Contracts.DTO;
using BicycleStore.Web.Models;

namespace BicycleStore.Web.Mappings
{
    public class MainWebProfile : Profile
    {
        public MainWebProfile()
        {
            CreateMap<BicycleDto, BicycleViewModel>();
            CreateMap<BicycleTypeDto, BicycleTypeViewModel>();
            CreateMap<BrandDto, BrandViewModel>();
            CreateMap<PhotoDto, PhotoViewModel>();
            CreateMap<ConfigurationDto, ConfigurationViewModel>();
            CreateMap<ColorDto, ColorViewModel>();
        }
    }
}