﻿using System;

namespace BicycleStore.Web.Models
{
    public class BicycleTypeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}