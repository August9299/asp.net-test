﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BicycleStore.Data.Contracts.Enums;

namespace BicycleStore.Web.Models
{
    public class BicycleViewModel
    {
        public bool IsAdding { get; set; }

        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Model Name")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public BicycleTypeViewModel BicycleType { get; set; }

        [Required]
        public Sex Sex { get; set; }

        [Required]
        [Range(10, 35)]
        public float WheelDiameter { get; set; }

        [Required]
        public BrandViewModel Brand { get; set; }

        [Required]
        [Range(0, 1000000)]
        public decimal Price { get; set; }

        public string Description { get; set; }

        public IEnumerable<BicycleTypeViewModel> AllBicycleTypes { get; set; }
        public IEnumerable<BrandViewModel> AllBrands { get; set; }

        public string MainPhoto { get; set; }

        public IEnumerable<PhotoViewModel> AllPhotos { get; set; }

        public IEnumerable<ConfigurationViewModel> Configurations { get; set; }        
    }
}
 