﻿using System;

namespace BicycleStore.Web.Models
{
    public class BrandViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}