﻿using System;

namespace BicycleStore.Web.Models
{
    public class ColorViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}