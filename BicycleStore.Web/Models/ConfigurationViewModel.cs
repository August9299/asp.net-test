﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BicycleStore.Web.Models
{
    public class ConfigurationViewModel
    {
        public bool IsAdding { get; set; }
        public Guid Id { get; set; }

        [Required]    
        public Guid BicycleId { get; set; }

        [Required]
        [Range(10, 30)]
        public float FrameSize { get; set; }

        [Required]
        public int Availability { get; set; }
    }
}