﻿using System;

namespace BicycleStore.Web.Models
{
    public class PhotoViewModel
    {
        public Guid Id { get; set; }        
        public Guid BicycleId { get; set; }
        public bool IsMain { get; set; }
    }
}