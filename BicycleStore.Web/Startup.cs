﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BicycleStore.Web.Startup))]

namespace BicycleStore.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}