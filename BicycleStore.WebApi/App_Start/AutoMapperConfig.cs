﻿using AutoMapper;

namespace BicycleStore.WebApi
{
    public class AutoMapperConfig
    {
        public static void AddProfiles()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Logic.BLL.Mappings.MainBllProfile>();
            });
        }
    }
}