using BicycleStore.Logic.BLL.Services;
using BicycleStore.Logic.Contracts.Services;
using Microsoft.Practices.Unity;
using System.Web.Http;
using BicycleStore.Data.Contracts;
using Unity.WebApi;
using BicycleStore.Data.DAL;

namespace BicycleStore.WebApi
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IBicycleService, BicycleService>();
            container.RegisterType<IConfigurationService, ConfigurationService>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);            
        }
    }
}