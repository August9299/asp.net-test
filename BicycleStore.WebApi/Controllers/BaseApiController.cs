﻿using System.Web.Http;
using System.Web.Http.Cors;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.WebApi.Controllers
{
    [EnableCors(origins: "http://localhost:58613", headers: "*", methods: "*")]
    public class BaseApiController : ApiController
    {
        protected IBicycleService BicycleService { get; }
        protected IConfigurationService ConfigurationService { get; }

        public BaseApiController(IBicycleService bicycleService, IConfigurationService configurationService)
        {
            BicycleService = bicycleService;
            ConfigurationService = configurationService;
        }
    }
}