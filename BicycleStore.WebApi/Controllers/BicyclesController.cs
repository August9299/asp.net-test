﻿using System;
using System.Diagnostics;
using System.Web.Http;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.WebApi.Controllers
{
    
    public class BicyclesController : BaseApiController
    {
        public BicyclesController(IBicycleService bicycleService, IConfigurationService configurationService) : base(bicycleService, configurationService)
        {
        }

        [HttpPost]        
        public IHttpActionResult Save(BicycleRequestModel model)
        {            
            try
            {
                var bicycleDto = BicycleService.SaveBicycle(model);
                return Ok(bicycleDto.Id);
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception: {e.Message}");
                Debug.WriteLine($"Inner: {e.InnerException?.Message}");
                return BadRequest();
            }            
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                BicycleService.DeleteBicycle(id);
                return Ok();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception: {e.Message}");
                Debug.WriteLine($"Inner: {e.InnerException?.Message}");
                return BadRequest();
            }
        }        
    }
}
