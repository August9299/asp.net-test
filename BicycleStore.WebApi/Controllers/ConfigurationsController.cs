﻿using System;
using System.Diagnostics;
using System.Web.Http;
using BicycleStore.Data.Contracts.RequestModels;
using BicycleStore.Logic.BLL.Services;
using BicycleStore.Logic.Contracts.Services;

namespace BicycleStore.WebApi.Controllers
{
    public class ConfigurationsController : BaseApiController
    {        
        [HttpPost]
        public IHttpActionResult Save(ConfigurationRequestModel model)
        {
            try
            {
                var configurationDto = ConfigurationService.SaveConfiguration(model);
                return Ok(configurationDto.Id);
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception: {e.Message}");
                Debug.WriteLine($"Inner: {e.InnerException?.Message}");
                return BadRequest();
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                ConfigurationService.DeleteConfiguration(id);
                return Ok();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Exception: {e.Message}");
                Debug.WriteLine($"Inner: {e.InnerException?.Message}");
                return BadRequest();
            }
        }

        public ConfigurationsController(IBicycleService bicycleService, IConfigurationService configurationService) : base(bicycleService, configurationService)
        {
        }
    }
}
